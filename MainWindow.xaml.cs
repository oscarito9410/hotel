﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MySql.Data.MySqlClient;
using Microsoft.Win32;
using System.Data;
using Becarios.View;
namespace Becarios
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
     
         
        private ViewModel.ViewModelUsuario viewModel = new ViewModel.ViewModelUsuario();
        public MainWindow()
        {
           
            InitializeComponent();
            this.DataContext =viewModel;
            this.suscribeEvent();
            this.Loaded += (s, args) =>
            {
               // new ClientesView(true).Show();
                // this.checkConnection();
            };
           
        }

        private async  void checkConnection()
        {
            var controller=await this.ShowProgressAsync("Iniciando conexión con servidor", "Estableciendo conexión con el servidor");
            if (await new Common.ConnectionManager().test() == true)
            {
                controller.SetMessage("Conexión establecida exitosamente");
                await Task.Delay(TimeSpan.FromSeconds(3));
                await controller.CloseAsync();
            }
            else
            {
                controller.SetMessage("Ha ocurrido un error al intentar conectar con servidor, conectando con servidor local");
                await Task.Delay(TimeSpan.FromSeconds(3));
                await controller.CloseAsync();
            }
        }

        private  void suscribeEvent()
        {
            viewModel.OnMessageDailogShowEvent += (s, args) =>
            {
                this.ShowMessageAsync(args.Title, args.Content);
            };
            viewModel.OnWindowActionEvent+= (s, args) =>
            {
                if (args.IsClose == true) {
                    args.MWindow.Show();
                    this.Close();
                }
            };

            this.comboTipo.ItemsSource = new Common.Keys().tiposUsuarios;
        }
       
        private void passwordLogin_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.viewModel.password = passwordLogin.Password;
        }

        private void passwordRegistrar_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.viewModel.passwordRegistrar = passwordRegistrar.Password;
        }

        private void passwordConfirmar_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.viewModel.passwordConfirmar = passwordConfirmar.Password;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (combo != null)
            {
                if (combo.SelectedItem != null)
                {
                    this.viewModel.tipo = combo.SelectedItem.ToString();
                }
            }
        }
    }
}
