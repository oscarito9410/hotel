﻿using MahApps.Metro.SimpleChildWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Becarios.View
{
    /// <summary>
    /// Interaction logic for ClienteHabitacionDetalle.xaml
    /// </summary>
    public partial class ClienteHabitacionDetalle : ChildWindow
    {
        private int num_habitacion { get; set; }
        public ClienteHabitacionDetalle(int num_habitacion)
        {
            InitializeComponent();
            this.num_habitacion = num_habitacion;
            this.getCliente();
        }

        private async void getCliente()
        {
            var todosClientes=await new Bd.CrudCliente().getAll(num_habitacion);
            var cliente = todosClientes.FirstOrDefault();
            if (cliente != null)
            {
                tvNombre.Text = cliente.nombre;
                tvEntrada.Text = cliente.dateEntrada.ToLongDateString();
                tvdias.Text = cliente.diasEstancia.ToString();
            }
            var habitaciones = new Bd.CrudHabitacion().getAll();
            var habitacion = habitaciones.Where(x => x.numero == num_habitacion).FirstOrDefault();
            if (habitacion != null)
            {
                tvPrecio.Text = String.Format("${0}", habitacion.precio);
                tvTipo.Text = habitacion.tamanio;
            }
        }

        
    }
}
