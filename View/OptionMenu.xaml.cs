﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls.Dialogs;
namespace Becarios.View
{
    /// <summary>
    /// Interaction logic for OptionMenu.xaml
    /// </summary>
    public partial class OptionMenu : MetroWindow
    {
        private bool tipo { get; set; }
        public OptionMenu(String tipoStr)
        {
            InitializeComponent();
            this.tvTipo.Text = String.Format("TIPO:{0}", tipoStr);
            this.tipo = new Common.Keys().getIsAdmin(tipoStr);
            this.Loaded += (s, args) => {

                bool avaliable = new Common.ConnectionManager().isAvaliable();

                if (avaliable == true)
                {
                    this.tvConexion.Text = "CONECTADO CON EL SERVIDOR";
                    this.tvConexion.Foreground = new SolidColorBrush(Colors.Green);
                }
                else
                {
                    this.tvConexion.Text = "NO SE PUDO CONECTAR CON EL SERVIDOR";
                    this.tvConexion.Foreground =new SolidColorBrush(Colors.Red);
                }
          
                   
                
            };
        }
        public OptionMenu()
        {
            InitializeComponent();
            this.tvTipo.Text = String.Empty;
            this.panelUsuario.Visibility = Visibility.Collapsed;
        }

        private void tileCliente_Click(object sender, RoutedEventArgs e)
        {
            new ClientesView(tipo).Show();
            this.Close();

        }

        private void tileHabitaciones_Click(object sender, RoutedEventArgs e)
        {
           
            new Habitaciones().Show();
            this.Close();

        }

        private void tileReservaciones_Click(object sender, RoutedEventArgs e)
        {
            new ProductosView().Show();
            this.Close();
        }

        private void tileRestaurante_Click(object sender, RoutedEventArgs e)
        {
            new Restaurante().Show();
            this.Close();

        }

        private async void tileExit_Click(object sender, RoutedEventArgs e)
        {
           var result=await this.ShowMessageAsync("Salir", "¿Estás seguro de salir?", MessageDialogStyle.AffirmativeAndNegative);
            if (result == MessageDialogResult.Affirmative)
            {
                new MainWindow().Show();
                this.Close();
            }
        }

        private void tileConsultas_Click(object sender, RoutedEventArgs e)
        {
            new ReporteGastos().Show();
            this.Close();

        }

        private void btnUpadte_Click(object sender, RoutedEventArgs e)
        {
            this.checkConnection();
        }

        private async void checkConnection()
        {
            var controller = await this.ShowProgressAsync("Iniciando conexión con servidor", "Estableciendo conexión con el servidor");
            if (await new Common.ConnectionManager().test() == true)
            {
                controller.SetMessage("Conexión establecida exitosamente");
                await Task.Delay(TimeSpan.FromSeconds(4));
                this.tvConexion.Text = "CONECTADO CON EL SERVIDOR";
                this.tvConexion.Foreground = new SolidColorBrush(Colors.Green);
                await controller.CloseAsync();
            }
            else
            {
                controller.SetMessage("Ha ocurrido un error al intentar conectar con servidor");
                await Task.Delay(TimeSpan.FromSeconds(4));
                this.tvConexion.Text = "NO SE PUDO CONECTAR CON EL SERVIDOR";
                this.tvConexion.Foreground = new SolidColorBrush(Colors.Red);
                await controller.CloseAsync();
            }

          

        }
    }
}
