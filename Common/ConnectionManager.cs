﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Becarios.Common
{
    public class ConnectionManager
    {
        /// <summary>
        /// Hay conexíon disponible
        /// </summary>
        public bool isAvaliable()
        {
            return System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
        }

        /// <summary>
        /// Método para testear conexión con servidor
        /// </summary>
        /// <returns></returns>
        public async Task<bool> test()
        {
            bool res = false;
            //No hay conexión a internet
            if (isAvaliable() == false)
            {
                res = false;
                return res;
            }

            MySqlConnectionStringBuilder conn = new MySqlConnectionStringBuilder();
            conn.Server = "sql5.freemysqlhosting.net";
            conn.Database = "sql5109508";
            conn.UserID = "sql5109508";
            conn.Password = "yE7QC3JWAA";
            conn.Port = 3306;

            String dbPath = "Data Source = localhost; Database = hotelcolonia; Uid = root; Password = 1234";
            using (MySqlConnection MidbConexion = new MySqlConnection(dbPath))
            {
                try
                {
                    MidbConexion.Open();
                    res = true;
                }
                catch (Exception)
                {
                    res = false;
                }
            }

            return res;
        }
    }
}
