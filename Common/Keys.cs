﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Becarios.Common
{
    public class Keys
    {
        public Dictionary<String, int> Habitaciones = new Dictionary<String, int>();
        public List<String> ProductosCategoria = new List<String>();
        public List<String> tiposUsuarios = new List<String>();
        public Keys()
        {
            Habitaciones.Add("Sencilla", 690);
            Habitaciones.Add("Doble", 820);
            Habitaciones.Add("Cuadruple", 1130);
            Habitaciones.Add("Suite", 1300);
            ProductosCategoria.Add("Entradas");
            ProductosCategoria.Add("Bebidas");
            ProductosCategoria.Add("Postres");
            ProductosCategoria.Add("Platillos");
            tiposUsuarios.Add("USUARIO");
            tiposUsuarios.Add("ADMIN");

        }

        public bool getIsAdmin(String tipo)
        {
            return tipo.Equals("ADMIN") ? true : false;
        }
    }
}
