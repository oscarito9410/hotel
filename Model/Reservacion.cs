﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Becarios.Model
{
    public class Reservacion : ViewModel.ViewModelBase
    {

        private int _id;
        public int id
        {
            get

            {
                return this._id;
            }
            set

            {
                this._id = value;
                this.RaisePropertyChanged("id");
            }
        }
        private int _numHabitacion;
        public int numHabitacion
        {
            get
            {
                return this._numHabitacion;
            }
            set
            {
                this._numHabitacion = value;
                this.RaisePropertyChanged("numHabitacion");
            }
        }

        private int _claveReservacion;
            public int claveReservacion
            {
                get
                {
                    return this._claveReservacion;
                }
                set
                {
                    this._claveReservacion = value;
                    this.RaisePropertyChanged("claveReservacion");
                }
            }



            private DateTime _diaEntrada;
            public  DateTime diaEntrada
            {
                get
                {
                    return this._diaEntrada;
                }
                set
                {
                    this._diaEntrada = value;
                    this.RaisePropertyChanged("diaEntrada");
                }
            }

            private DateTime _diaSalida;
            public DateTime diaSalida
            {
                get
                {
                    return this._diaSalida;
                }
                set
                {
                    this._diaSalida = value;
                    this.RaisePropertyChanged("diaSalida");
                }
            }

        private TimeSpan _horaEntrada;
        public TimeSpan horaEntrada
        {
            get
            {
                return this._horaEntrada;
            }
            set
            {
                this._horaEntrada = value;
                this.RaisePropertyChanged("horaEntrada");
            }
        }
        private TimeSpan _horaSalida;
        public TimeSpan horaSalida
        {
            get
            {
                return this._horaSalida;
            }
            set
            {
                this._horaSalida = value;
                this.RaisePropertyChanged("horaSalida");
            }
        }


    }
}
