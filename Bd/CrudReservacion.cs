﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Becarios.Bd
{
    public class CrudReservacion : ICrudHelper
    {

        private int numHabitacion
        {
            get;
            set;
        }
        public CrudReservacion() { }
        public CrudReservacion(int numHabitacion)
        {
            this.numHabitacion = numHabitacion;
        }
        
        public async Task<ObservableCollection<Model.Reservacion>> getReservacion(int idCliente)
        {
            ObservableCollection<Model.Reservacion> listReservaciones = new ObservableCollection<Model.Reservacion>();

            using (MySqlConnection MidbConexion = new MySqlConnection(this.dbPath))
            {
                MidbConexion.Open();
                MySqlCommand miComando = new MySqlCommand()
                {
                    Connection = MidbConexion,
                    CommandText = String.Format("SELECT*FROM control_reservacion WHERE id_cliente='{0}'", idCliente)
                };
                var reader = await miComando.ExecuteReaderAsync();
                while (reader.Read())
                {
                    Model.Reservacion reservacion = new Model.Reservacion();

                    reservacion.claveReservacion = Convert.ToInt32(reader["clv_reservacion"]);
                    reservacion.diaSalida = Convert.ToDateTime(reader["dia_salida"]);
                    reservacion.diaEntrada = Convert.ToDateTime(reader["dia_entrada"]);
                    reservacion.horaEntrada = TimeSpan.Parse(reader["hora_entrada"].ToString());
                    reservacion.horaSalida = TimeSpan.Parse(reader["hora_salida"].ToString());

                    listReservaciones.Add(reservacion);
                }
                return listReservaciones;

            }
        }

        public override Task<bool> addAsync(object table)
        {
            throw new NotImplementedException();
        }

        public override async Task<bool> deleteAsync(object table)
        {
            var reservacion = this.getCastReservacion(table);
            using (MySqlConnection MidbConexion = new MySqlConnection(this.dbPath))
            {
                MySqlCommand miComando = new MySqlCommand()
                {
                    Connection = MidbConexion,
                    CommandText = "DELETE FROM control_reservacion WHERE clv_reservacion =?clv_reservacion"
                };
                MidbConexion.Open();
                miComando.Parameters.AddWithValue("?clv_reservacion", reservacion.claveReservacion);
                await miComando.ExecuteNonQueryAsync();

            }
            await new CrudHabitacion().updateAsync(this.numHabitacion, false);
            return true;
        }
        public Model.Reservacion getCastReservacion(object table)
        {

            return table as Model.Reservacion;
        }

        public override Task<bool> exists(object table)
        {
            throw new NotImplementedException();
        }

        public override Task<bool> updateAsync(object table)
        {
            throw new NotImplementedException();
        }
    }
}
