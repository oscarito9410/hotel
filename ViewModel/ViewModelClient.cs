﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Becarios.Bd;
using System.Collections.ObjectModel;
using MahApps.Metro.Controls.Dialogs;
using Becarios.Model;
using Becarios.Common;
using System.Windows;
using MahApps.Metro.Controls;

namespace Becarios.ViewModel
{
    public class ViewModelClient: Model.Cliente,ICustomCommand
    {
        private Visibility _hasReservation;
        public Visibility hasReservation
        {
            get
            {
                return this._hasReservation;
            }
            set
            {
                this._hasReservation = value;
                if (value== Visibility.Visible)
                {
                    this.hasReservationT = Visibility.Collapsed;
                }
                else
                {
                    this.hasReservationT = Visibility.Visible;
                }
                this.RaisePropertyChanged("hasReservation");
            }
        }
        private Visibility _hasReservationT;
        public Visibility hasReservationT
        {
            get
            {
                return this._hasReservationT;
            }
            set
            {

                this._hasReservationT = value;
                this.RaisePropertyChanged("hasReservationT");
            }
        }

        private bool _isOpen;
        public bool isOpen
        {
            get
            {
                return this._isOpen;
            }
            set
            {
                this._isOpen = value;
                this.RaisePropertyChanged("isOpen");
            }
        }
        private Habitacion _selectedHabitacion;
        public  Habitacion selectedHabitacion
        {
            get
            {
                return this._selectedHabitacion;
            }
            set
            {
                if (value != null)
                {
                    this._selectedHabitacion = value;
                    this.selectedPrecio = value.precio;
                    this.RaisePropertyChanged("selectedHabitacion");
                }
            }
        }
        private double _selectedPrecio;
        public double selectedPrecio
        {
            get
            {
                return this._selectedPrecio;
            }
            set
            {
                this._selectedPrecio = value;
                this.RaisePropertyChanged("selectedPrecio");
            }
        }
        private ObservableCollection<Cliente> allClientes;
        private ObservableCollection<Reservacion> _listReservacion;
        public ObservableCollection<Reservacion>listReservacion
        {
            get
            {
                return this._listReservacion;
            }
            set
            {
                this._listReservacion = value;
                this.RaisePropertyChanged("listReservacion");
            }
        }
        private ObservableCollection<Habitacion> _listHabitacionesDisponibles;
        public ObservableCollection<Habitacion> listHabitacionesDisponibles
        {
            get
            {
                return this._listHabitacionesDisponibles;
            }
            set

            {
                this._listHabitacionesDisponibles = value;
                this.RaisePropertyChanged("listHabitacionesDisponibles");
            }
        }
        private int max = 0;
        private int min = 0;
        public ViewModelClient(bool isAdmin)
        {
            this.dateEntrada = DateTime.Now;
            this.dateSalida = DateTime.Now.AddDays(1);
            this.commandSave = new DelegateCommand(save);
            this.crudHelper = new CrudCliente();
            this.commandAddNew = new DelegateCommand(addNew);
            this.commandOpenDelete = new DelegateCommand(openPopupDelete);
            this.commandSiguiente = new DelegateCommand(this.siguiente);
            this.commandAnterior = new DelegateCommand(this.anterior);
            this.commandDelete = new DelegateCommand(this.delete);
            this.commandGoBack = new DelegateCommand(this.goBack);
            this.commandDeleteFromList = new DelegateCommand<object>(deleteFromList);
     
            this.getAllClientes();
            this.isAdmin = isAdmin;

            
           
        }

        private async void deleteFromList(object paramter)
        {


            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            MessageDialogResult result= await metroWindow.ShowMessageAsync("CANCELAR", "¿ESTAS SEGURO DE CANCELAR LA RESERVACIÓN?",MessageDialogStyle.AffirmativeAndNegative);
            if (result == MessageDialogResult.Affirmative)
            {
                var currentReservacion = this.listReservacion[0];
                if(await new CrudReservacion(currentReservacion.numHabitacion).deleteAsync(currentReservacion)== true)
                {
                    this.addNew();
                }

            }

        }

        private async void updateListCliente()
        {
            var crudClient = this.crudHelper as CrudCliente;
            this.allClientes = await crudClient.getAll();
        }
        private async void getAllClientes()
        {
            try {
                var crudClient = this.crudHelper as CrudCliente;
                this.allClientes = await crudClient.getAll();
                this.listHabitacionesDisponibles = new ObservableCollection<Model.Habitacion>();
                this.min = await this.crudHelper.getMinAsync("cliente", "id_cliente");
                this.max = await this.crudHelper.getMaxAsync("cliente", "id_cliente");
                this.id = min;
                this.navigate(this.id, false);

              
            }
            catch(Exception ex)
            {
                this.OnMessageDialogReceived("ERROR", ex.Message);
            }
        }
        public async void addNew()
        {
            this.id =  await this.crudHelper.getMaxAsync("cliente", "id_cliente");
            this.max = this.id;
            this.nombre = String.Empty;
            this.dateEntrada = DateTime.Now;
            this.dateSalida = DateTime.Now.AddDays(1);
            this.origen = String.Empty;
            this.email = String.Empty;
            this.acompaniantes = 0;
            this.tarjeta = String.Empty;
            this.telefono = String.Empty;
            this.diasEstancia = 0;
            this.clvReservacion = await this.crudHelper.getMaxAsync("control_reservacion", "clv_reservacion");
            this.updateListCliente();
            this.hasReservation = Visibility.Collapsed;
        }
        public async void navigate(int id, bool anterior)
        {
            Model.Cliente cliente =  allClientes.Where(x => x.id == id).FirstOrDefault();

            //El cliente tiene clave de reservacion;
            if (cliente != null) {
                this.id = cliente.id;
                this.nombre = cliente.nombre;
                this.dateEntrada = cliente.dateEntrada;
                this.dateSalida = cliente.dateSalida;
                this.origen = cliente.origen;
                this.email = cliente.email;
                this.telefono = cliente.telefono;
                this.diasEstancia = cliente.diasEstancia;
                this.acompaniantes = cliente.acompaniantes;
                this.tarjeta = cliente.tarjeta;
                this.clvReservacion = cliente.clvReservacion;
                this.hasReservation = Visibility.Visible;
                this.listReservacion = new ObservableCollection<Reservacion>();
                this.listReservacion.Add(new Reservacion()
                {
                    claveReservacion = cliente.clvReservacion,
                    diaEntrada = cliente.dateEntrada,
                    diaSalida = cliente.dateSalida,
                    numHabitacion = cliente.numHabitacion,
                    id = cliente.id
                });
                this.commandShowDetalles = new DelegateCommand(() => {
                    this.OnChildWindowShow(cliente.numHabitacion);
                });

            }
            else
            {
                //clientes sin reservaciones
                var noReservation = await new CrudCliente().getJustClient();
                var singleCliente=noReservation.Where(x => x.id == id).FirstOrDefault();
                if (singleCliente != null)
                {
                    this.id = singleCliente.id;
                    this.nombre = singleCliente.nombre;
                    this.dateEntrada = singleCliente.dateEntrada;
                    this.dateSalida = singleCliente.dateSalida;
                    this.diasEstancia = singleCliente.diasEstancia;
                    this.tarjeta = singleCliente.tarjeta;
                    this.origen =singleCliente.origen;
                    this.email = singleCliente.email;
                    this.telefono = singleCliente.telefono;
                    this.hasReservation = Visibility.Collapsed;
                }
            }
        }
        public async void save()
        {

           

            if(String.IsNullOrEmpty(nombre))
            {
                this.OnMessageDialogReceived(Settings.titulo, "Ingresa tu nombre");
            }
            else if(String.IsNullOrEmpty(email))
            {
                this.OnMessageDialogReceived(Settings.titulo, "Ingresa tu email");
            }
            else if(String.IsNullOrEmpty(tarjeta) || tarjeta.Length<4)
            {
                this.OnMessageDialogReceived(Settings.titulo, "Numero tarjeta no valido");
            }
           
            else if(!Regex.IsMatch(this.email, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                this.OnMessageDialogReceived(Settings.titulo, "Email no valido");
            }
            else if(String.IsNullOrEmpty(telefono))
            {
                this.OnMessageDialogReceived(Settings.titulo, "Ingresa telefono");
            }
            else if(String.IsNullOrEmpty(origen))
            {
                this.OnMessageDialogReceived(Settings.titulo, "Ingresa el origen");
            }
            else if (this.selectedHabitacion == null)
            {
                this.OnMessageDialogReceived(Settings.titulo, "Selecciona la habitación del usuario");
            }
            else
            {

                if (await this.crudHelper.isUpdate("cliente", "id_cliente", this.id) == true)
                {
                    await this.crudHelper.updateAsync(this);
                    this.OnMessageDialogReceived(Settings.titulo, Settings.getActualizado("Cliente"));
                    this.getAllClientes();
                }
                else
                {
                
                    await this.crudHelper.addAsync(this);
                    this.OnMessageDialogReceived(Settings.titulo,Settings.getInsertado("Cliente"));
                    this.getAllClientes();
                }
               
               
            }
         
        }
        public async void delete()
        {
            if (isAdmin == false)
            {
                this.OnMessageDialogReceived(Settings.titulo, "Has entrado como usuario, no puedes eliminar");
                return;
            }

            if (await this.crudHelper.deleteAsync(this)==true)
            {
                this.isOpen = false;
                this.addNew();
            }
        }
        public void siguiente()
        {
            if (id < max - 1)
            {
                this.id++;
                this.navigate(id,false);
            }
        }
        public void anterior()
        {
            if (id > min)
            {
                this.id--;
                this.navigate(id,true);
            }
        }
        public void openPopupDelete()
        {
           
                if (this.id == max)
                {
                    OnMessageDialogReceived(Settings.titulo, Settings.imposibleBorrar);
                    return;
                }

                this.isOpen = !this.isOpen;
         }

        public void goBack()
        {
            this.OnWindowActionReceived(false, new View.OptionMenu());
        }

        public ICommand commandAnterior
        {
            get; set;
        }
        public ICommand commandSiguiente
        {
            get;  set;
        }
        public ICommand commandAddNew
        {
            get; set;
        }
        public ICommand commandOpenDelete
        {
            get;  set;
        }
        public ICommand commandDelete
        {
            get;  set;
        }
        public ICommand commandSave
        {
            get;  set;
        }
        public ICommand commandDeleteFromList
        {
            get;
            private set;
        }
        public ICommand commandShowDetalles
        {
            get;
            private set;
        }
        public ICommand commandGoBack
        {
            get; set;
        }
    }
}
